import { Component } from '@angular/core';
import { faDashboard, faLocation,faCoffee, faShop, faBox, faMobile,faMoneyBill, faContactBook,faUsers, faHome,faChartBar,faHand, faUser, faMobilePhone, faPencil,  faHandPaper, faHandsHelping} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent {
  faDashboard = faDashboard;
  faLocation = faLocation;
  faShop = faShop;
  faMoneyBill = faMoneyBill;
  faBox = faBox;
  faContactBook = faContactBook;
  faUsers = faUsers;
  faHome = faHome;
  faCoffee = faCoffee;
  faMobile = faMobile;
  faChartBar = faChartBar
  faHand  = faHand;
  faUser = faUser;
  faMobilePhone = faMobilePhone;
  faPencil = faPencil;
  faHandPaper = faHandPaper;
  faHandsHelping = faHandsHelping;
}
